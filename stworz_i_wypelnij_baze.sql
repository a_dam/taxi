/*
Created		2015-11-20
Modified		2015-12-12
Project		
Model		
Company		
Author		
Version		
Database		Oracle 10g 
*/



Drop table Kursy
/
Drop table Pojazdy
/
Drop table Klienci
/
Drop table Ulice
/
Drop table Statusy_kursow
/
Drop table Kolory_pojazdow
/
Drop table Kategorie_pojazdow
/





Create table Klienci (
	Klienci_Id Integer NOT NULL ,
	Imie Varchar2 (20) NOT NULL ,
	Nazwisko Varchar2 (50) NOT NULL ,
	Nr_telefonu Varchar2 (12) NOT NULL  UNIQUE ,
	E_mail Varchar2 (50) NOT NULL  UNIQUE ,
	Login Varchar2 (20) NOT NULL  UNIQUE ,
	Haslo Varchar2 (30) NOT NULL ,
primary key (Klienci_Id) 
) 
/

Create table Pojazdy (
	Pojazdy_Id Integer NOT NULL ,
	Marka Varchar2 (15) NOT NULL ,
	Model Varchar2 (15) NOT NULL ,
	Nr_rejestracyjny Varchar2 (8) NOT NULL  UNIQUE ,
	Nr_boczny Varchar2 (4) NOT NULL  UNIQUE ,
	Liczba_miejsc Integer NOT NULL ,
	Kategoria_Id Integer NOT NULL ,
	Kolor_Id Integer NOT NULL ,
	Kierowca_imie Varchar2 (30) NOT NULL ,
	Kierowca_nazwisko Varchar2 (30) NOT NULL ,
primary key (Pojazdy_Id) 
) 
/

Create table Ulice (
	Ulice_Id Integer NOT NULL ,
	Nazwa Varchar2 (30) NOT NULL  UNIQUE ,
primary key (Ulice_Id) 
) 
/

Create table Kursy (
	KursyId Integer NOT NULL  UNIQUE ,
	Data Date NOT NULL ,
	Godzina Varchar2 (5) NOT NULL ,
	Klienci_Id Integer NOT NULL ,
	Status_Id Integer NOT NULL ,
	Skad Integer NOT NULL ,
	Dokad Integer NOT NULL ,
	Pojazdy_Id Integer NOT NULL ,
primary key (KursyId,Klienci_Id,Skad,Dokad,Pojazdy_Id) 
) 
/

Create table Kategorie_pojazdow (
	Kategoria_Id Integer NOT NULL ,
	Kategoria Varchar2 (30) NOT NULL ,
primary key (Kategoria_Id) 
) 
/

Create table Kolory_pojazdow (
	Kolor_Id Integer NOT NULL ,
	Kolor Varchar2 (30) NOT NULL  UNIQUE ,
primary key (Kolor_Id) 
) 
/

Create table Statusy_kursow (
	Status_Id Integer NOT NULL ,
	Status Varchar2 (30) NOT NULL  UNIQUE ,
primary key (Status_Id) 
) 
/




Alter table Kursy add  foreign key (Klienci_Id) references Klienci (Klienci_Id) -- foreign key (Klienci_Id) 
/

Alter table Kursy add  foreign key (Pojazdy_Id) references Pojazdy (Pojazdy_Id) -- foreign key (Pojazdy_Id)
/

Alter table Kursy add  foreign key (Skad) references Ulice (Ulice_Id) -- foreign key (Ulice_Id)
/

Alter table Kursy add  foreign key (Dokad) references Ulice (Ulice_Id) -- foreign key (Ulice_Id)
/

Alter table Pojazdy add  foreign key (Kategoria_Id) references Kategorie_pojazdow (Kategoria_Id) -- foreign key (Kategoria_Id)
/

Alter table Pojazdy add  foreign key (Kolor_Id) references Kolory_pojazdow (Kolor_Id) -- foreign key (Kolor_Id)
/

Alter table Kursy add  foreign key (Status_Id) references Statusy_kursow (Status_Id) -- foreign key (Status_Id)
/


INSERT INTO ULICE VALUES (1,'11 Batalionu Dowodzenia');
INSERT INTO ULICE VALUES (2,'Armii Krajowej');
INSERT INTO ULICE VALUES (3,'Asnyka');
INSERT INTO ULICE VALUES (4,'Augustynów');
INSERT INTO ULICE VALUES (5,'Boczna');
INSERT INTO ULICE VALUES (6,'Chrobrego');
INSERT INTO ULICE VALUES (7,'Kędzierzawego');
INSERT INTO ULICE VALUES (8,'Bolesławiecka');
INSERT INTO ULICE VALUES (9,'Pluty');
INSERT INTO ULICE VALUES (10,'Bracka');
INSERT INTO ULICE VALUES (11,'Browarniania');
INSERT INTO ULICE VALUES (12,'Brzozowa');
INSERT INTO ULICE VALUES (13,'Buczka');
INSERT INTO ULICE VALUES (14,'Chopina');
INSERT INTO ULICE VALUES (15,'Akacjowa');
INSERT INTO ULICE VALUES (16,'Cicha');
INSERT INTO ULICE VALUES (17,'Czereśniowa');
INSERT INTO ULICE VALUES (18,'Dąbrowskiego');
INSERT INTO ULICE VALUES (19,'Dębowa');
INSERT INTO ULICE VALUES (20,'Długa');
INSERT INTO ULICE VALUES (21,'Dolna');
INSERT INTO ULICE VALUES (22,'Drzewna');
INSERT INTO ULICE VALUES (23,'Dworcowa');
INSERT INTO ULICE VALUES (24,'Fabryczna');
INSERT INTO ULICE VALUES (25,'Findera');
INSERT INTO ULICE VALUES (26,'Fornalskiej');
INSERT INTO ULICE VALUES (27,'Fredry');
INSERT INTO ULICE VALUES (28,'Bema');
INSERT INTO ULICE VALUES (29,'Gimnazjalna');
INSERT INTO ULICE VALUES (30,'Górna');
INSERT INTO ULICE VALUES (31,'Graniczna');
INSERT INTO ULICE VALUES (32,'Halicka');
INSERT INTO ULICE VALUES (33,'Brodatego');
INSERT INTO ULICE VALUES (34,'II Armii WP');
INSERT INTO ULICE VALUES (35,'Jagiellońska');
INSERT INTO ULICE VALUES (36,'Jana Długosza');
INSERT INTO ULICE VALUES (37,'Jana Felbigera');
INSERT INTO ULICE VALUES (38,'Jana III Sobieskiego');
INSERT INTO ULICE VALUES (39,'Jana Pawła II');
INSERT INTO ULICE VALUES (40,'Jaśminowa');
INSERT INTO ULICE VALUES (41,'Joselewicza');
INSERT INTO ULICE VALUES (42,'Karliki');
INSERT INTO ULICE VALUES (43,'Karpińskiego');
INSERT INTO ULICE VALUES (44,'Kazimierza Wielkiego');
INSERT INTO ULICE VALUES (45,'Keplera');
INSERT INTO ULICE VALUES (46,'pl. Kilińskiego');
INSERT INTO ULICE VALUES (47,'pl. Klasztorny');
INSERT INTO ULICE VALUES (48,'Klonowa');
INSERT INTO ULICE VALUES (49,'Kochanowskiego');
INSERT INTO ULICE VALUES (50,'Kolejowa');
INSERT INTO ULICE VALUES (51,'Konarskiego');
INSERT INTO ULICE VALUES (52,'Konopnickiej');
INSERT INTO ULICE VALUES (53,'Końcowa');
INSERT INTO ULICE VALUES (54,'Kopernika');
INSERT INTO ULICE VALUES (55,'Koszarowa');
INSERT INTO ULICE VALUES (56,'Kościuszki');
INSERT INTO ULICE VALUES (57,'Kożuchowska');
INSERT INTO ULICE VALUES (58,'Kręta');
INSERT INTO ULICE VALUES (59,'pl. Królowej Jadwigi');
INSERT INTO ULICE VALUES (60,'Krótka');
INSERT INTO ULICE VALUES (61,'Księcia Henryka Wiernego');
INSERT INTO ULICE VALUES (62,'Księcia Józefa Poniatowskiego');
INSERT INTO ULICE VALUES (63,'Księżnej Żaganny');
INSERT INTO ULICE VALUES (64,'Kwiatowa');
INSERT INTO ULICE VALUES (65,'Leśna');
INSERT INTO ULICE VALUES (66,'Libelta');
INSERT INTO ULICE VALUES (67,'Lipowa');
INSERT INTO ULICE VALUES (68,'Lotników Alianckich');
INSERT INTO ULICE VALUES (69,'Łąkowa');
INSERT INTO ULICE VALUES (70,'Łukasiewicza');
INSERT INTO ULICE VALUES (71,'Łużycka');
INSERT INTO ULICE VALUES (72,'Mickiewicza');
INSERT INTO ULICE VALUES (73,'Mieszka I');
INSERT INTO ULICE VALUES (74,'Miodowa');
INSERT INTO ULICE VALUES (75,'Młynarska');
INSERT INTO ULICE VALUES (76,'Moniuszki');
INSERT INTO ULICE VALUES (77,'Morelowa');
INSERT INTO ULICE VALUES (78,'Myśliwska');
INSERT INTO ULICE VALUES (79,'Nadbobrze');
INSERT INTO ULICE VALUES (80,'Narutowicza');
INSERT INTO ULICE VALUES (81,'Niepodległości');
INSERT INTO ULICE VALUES (82,'Nocznickiego');
INSERT INTO ULICE VALUES (83,'Nowa');
INSERT INTO ULICE VALUES (84,'Nowogródzka');
INSERT INTO ULICE VALUES (85,'Nowotki');
INSERT INTO ULICE VALUES (86,'Obwodowa');
INSERT INTO ULICE VALUES (87,'Okrzei');
INSERT INTO ULICE VALUES (88,'pl. Orląt Lwowskich');
INSERT INTO ULICE VALUES (89,'Paderewskiego');
INSERT INTO ULICE VALUES (90,'Piaskowa');
INSERT INTO ULICE VALUES (91,'Piastowska');
INSERT INTO ULICE VALUES (92,'Piłsudskiego');
INSERT INTO ULICE VALUES (93,'Plater');
INSERT INTO ULICE VALUES (94,'Podgórna');
INSERT INTO ULICE VALUES (95,'Podmiejska');
INSERT INTO ULICE VALUES (96,'Pogodna');
INSERT INTO ULICE VALUES (97,'Polna');
INSERT INTO ULICE VALUES (98,'Pomorska');
INSERT INTO ULICE VALUES (99,'Poprzeczna');
INSERT INTO ULICE VALUES (100,'Porzeczkowa');	
INSERT INTO ULICE VALUES (101,'Północna');
INSERT INTO ULICE VALUES (102,'Prusa');
INSERT INTO ULICE VALUES (103,'Przyjaciół Żołnierza');
INSERT INTO ULICE VALUES (104,'Pstrowskiego');
INSERT INTO ULICE VALUES (105,'Pułaskiego');
INSERT INTO ULICE VALUES (106,'Wurtha');
INSERT INTO ULICE VALUES (107,'Reymonta');
INSERT INTO ULICE VALUES (108,'Robotnicza');
INSERT INTO ULICE VALUES (109,'Różana');
INSERT INTO ULICE VALUES (110,'Rybacka');
INSERT INTO ULICE VALUES (111,'Rynek');
INSERT INTO ULICE VALUES (112,'Rzeźnicka');
INSERT INTO ULICE VALUES (113,'Sienkiewicza');
INSERT INTO ULICE VALUES (114,'Słoneczna');
INSERT INTO ULICE VALUES (115,'Słowackiego');
INSERT INTO ULICE VALUES (116,'pl. Słowiański');
INSERT INTO ULICE VALUES (117,'Sosnowa');
INSERT INTO ULICE VALUES (118,'Spokojna');
INSERT INTO ULICE VALUES (119,'Sportowa');
INSERT INTO ULICE VALUES (120,'Spółdzielcza');
INSERT INTO ULICE VALUES (121,'Starowiejska');
INSERT INTO ULICE VALUES (122,'Staszica');
INSERT INTO ULICE VALUES (123,'Stroma');
INSERT INTO ULICE VALUES (124,'Strumykowa');
INSERT INTO ULICE VALUES (125,'Szkolna');
INSERT INTO ULICE VALUES (126,'Szlachetna');
INSERT INTO ULICE VALUES (127,'Szpitalna');
INSERT INTO ULICE VALUES (128,'Szprotawska');
INSERT INTO ULICE VALUES (129,'Śląska');
INSERT INTO ULICE VALUES (130,'Środkowa');
INSERT INTO ULICE VALUES (131,'św. Cecylii');
INSERT INTO ULICE VALUES (132,'św. Michała');
INSERT INTO ULICE VALUES (133,'Świerkowa');
INSERT INTO ULICE VALUES (134,'Tartakowa');
INSERT INTO ULICE VALUES (135,'Teatralna');
INSERT INTO ULICE VALUES (136,'Towarowa');
INSERT INTO ULICE VALUES (137,'Traugutta');
INSERT INTO ULICE VALUES (138,'Wałowa');
INSERT INTO ULICE VALUES (139,'Warszawska');
INSERT INTO ULICE VALUES (140,'Waryńskiego');
INSERT INTO ULICE VALUES (141,'Wesoła');
INSERT INTO ULICE VALUES (142,'Węglowa');
INSERT INTO ULICE VALUES (143,'Wiejska');
INSERT INTO ULICE VALUES (144,'Wiśniowa');
INSERT INTO ULICE VALUES (145,'Łokietka');
INSERT INTO ULICE VALUES (146,'Wodna');
INSERT INTO ULICE VALUES (147,'Wojska Polskiego');
INSERT INTO ULICE VALUES (148,'pl. Wolności');
INSERT INTO ULICE VALUES (149,'X-lecia PRL');
INSERT INTO ULICE VALUES (150,'Zacisze');
INSERT INTO ULICE VALUES (151,'Zamkowa');
INSERT INTO ULICE VALUES (152,'Zielona');
INSERT INTO ULICE VALUES (153,'Żarska');
INSERT INTO ULICE VALUES (154,'Żelazna');
INSERT INTO ULICE VALUES (155,'Żółkiewskiego');
INSERT INTO ULICE VALUES (156,'Żwirki i Wigury');

INSERT INTO KATEGORIE_POJAZDOW VALUES (1,'Sedan');
INSERT INTO KATEGORIE_POJAZDOW VALUES (2,'Kombi');
INSERT INTO KATEGORIE_POJAZDOW VALUES (3,'Liftback');
INSERT INTO KATEGORIE_POJAZDOW VALUES (4,'Minivan');
INSERT INTO KATEGORIE_POJAZDOW VALUES (5,'Hatchback');

INSERT INTO KOLORY_POJAZDOW VALUES (1,'Czarny');
INSERT INTO KOLORY_POJAZDOW VALUES (2,'Biały');
INSERT INTO KOLORY_POJAZDOW VALUES (3,'Zielony');
INSERT INTO KOLORY_POJAZDOW VALUES (4,'Niebieski');
INSERT INTO KOLORY_POJAZDOW VALUES (5,'Żółty');
INSERT INTO KOLORY_POJAZDOW VALUES (6,'Czerwony');
INSERT INTO KOLORY_POJAZDOW VALUES (7,'Srebrny');
INSERT INTO KOLORY_POJAZDOW VALUES (8,'Brązowy');
INSERT INTO KOLORY_POJAZDOW VALUES (9,'Fioletowy');
INSERT INTO KOLORY_POJAZDOW VALUES (10,'Granatowy');

INSERT INTO STATUSY_KURSOW VALUES (1,'Oczekujący');
INSERT INTO STATUSY_KURSOW VALUES (2,'Zakończony');
INSERT INTO STATUSY_KURSOW VALUES (3,'Anulowany');

INSERT INTO KLIENCI VALUES (0,'dyspozytorka','dyspozytorka','111111111','dyspozytorka@taxi.pl','dyspozytorka','T4k!');
INSERT INTO KLIENCI VALUES (1,'Barbara','Szatańska','788746458','b.szatanska@wp.pl','szatanka','szatanka');
INSERT INTO KLIENCI VALUES (2,'Andrzej','Pieczowski','875586233','a.pieczowski@onet.pl','pieczek','pieczek');
INSERT INTO KLIENCI VALUES (3,'Katarzyna','Glina','888458679','k.glina@interia.pl','glinianka','glinianka');
INSERT INTO KLIENCI VALUES (4,'Robert','Morski','606123792','r.morski@gmail.com','morski','morski');
INSERT INTO KLIENCI VALUES (5,'Edyta','Górnik','678924654','e.gornik@onet.pl','edzia','edzia');
INSERT INTO KLIENCI VALUES (6,'Jacek','Balcerzak','761301276','j.balcerzak@wp.pl','bokser','bokser');
INSERT INTO KLIENCI VALUES (7,'Milena','Starska','888411064','m.starska@wp.pl','milenka','milenka');
INSERT INTO KLIENCI VALUES (8,'Adam','Małamysz','888444555','a.malamysz@gmail.com','orzel','orzel');
INSERT INTO KLIENCI VALUES (9,'Beata','Dekla','788146458','b.dekla@wp.pl','beata','beata');
INSERT INTO KLIENCI VALUES (10,'Anna','Cieśla','68746821','a.ciesla@onet.pl','cieslowa','cieslowa');
INSERT INTO KLIENCI VALUES (11,'Paulina','Kurzajka','721645812','p.kurzajka@tvp.pl','kurzajka','kurzajka');
INSERT INTO KLIENCI VALUES (12,'Piotr','Kreska','746458111','p.kreska@tvp.pl','kreska','kreska');
INSERT INTO KLIENCI VALUES (13,'Zygmunt','Chajzerowski','68874644','z.chajzerowski@wp.pl','vizir','vizir');
INSERT INTO KLIENCI VALUES (14,'Joanna','Orleńska','874124658','j.orlenska@interia.pl','orlen','orlen');
INSERT INTO KLIENCI VALUES (15,'Ewelina','Lisek','621122458','e.lisek@gmail.com','lis','lis');
INSERT INTO KLIENCI VALUES (16,'Magdalena','Pomidor','711164581','m.pomidor@o2.pl','pomidorek','pomidorek');
INSERT INTO KLIENCI VALUES (17,'Jakub','Powiatowy','887111243','j.powiatowy@tvn.pl','tvn','tvn');
INSERT INTO KLIENCI VALUES (18,'Aleksandra','Norweg','828464512','a.norweg@wp.pl','ola','ola');
INSERT INTO KLIENCI VALUES (19,'Małgorzata','Rodzynek','604108958','m.rodzynek@o2.pl','rekawiczka','rekawiczka');
INSERT INTO KLIENCI VALUES (20,'Jarosław','Kaczorski','874624582','j.kaczorski@onet.pl','pis','pis');

INSERT INTO POJAZDY VALUES (0,'Taksówka', 'Widmo', 'FZG000', 'BRAK',0,1,1,'A','A');
INSERT INTO POJAZDY VALUES (1,'Renault','Laguna','FZG1457','2457',4,1,1,'Marcin','Dorociański');
INSERT INTO POJAZDY VALUES (2,'Ford','Mondeo','FZG09PJ','0022',4,1,2,'Janusz','Gajowski');
INSERT INTO POJAZDY VALUES (3,'Peugeot','307','FZG14782','1234',4,2,5,'Krzysztof','Globus');
INSERT INTO POJAZDY VALUES (4,'Opel','Astra','FZG00001','4567',4,1,7,'Tomasz','Kotecki');
INSERT INTO POJAZDY VALUES (5,'Renault','Scenic','FZG12345','7890',5,4,3,'Tomasz','Karolewski');
INSERT INTO POJAZDY VALUES (6,'Audi','A4','FZG78DW','5467',4,2,6,'Jan','Frytkowski');
INSERT INTO POJAZDY VALUES (7,'Volkswagen','Passat','FZG6476','9876',4,1,5,'Paweł','Małamysz');
INSERT INTO POJAZDY VALUES (8,'Citroen','C5','FZG75ZG','2582',4,3,10,'Cezary','Paznokieć');
INSERT INTO POJAZDY VALUES (9,'Volkswagen','Sharan','FZG34792','7410',5,4,8,'Artur','Żmija');
INSERT INTO POJAZDY VALUES (10,'Toyota','Avensis','FZG93254','3671',4,1,7,'Olaf','Lubaszanek');
INSERT INTO POJAZDY VALUES (11,'Volkswagen','Golf','FZG13FK','9634',4,5,8,'Maciej','Kościelny');
INSERT INTO POJAZDY VALUES (12,'Skoda','Octavia','FZG1010','9731',4,3,6,'Piotr','Adamczak');
INSERT INTO POJAZDY VALUES (13,'Fiat','Multipla','FZG27FS','1379',5,4,4,'Marek','Bukowicz');
INSERT INTO POJAZDY VALUES (14,'Mercedes-Benz','W221','FZG41SD','4682',4,1,2,'Michał','Żebro');
INSERT INTO POJAZDY VALUES (15,'BMW','E90','FZG87NH','8246',5,1,1,'Robert','Więckowski');

INSERT INTO KURSY VALUES(1,TO_DATE('2015-11-22','YYYY-MM-DD'),'11:11',1,2,41,78,1);
INSERT INTO KURSY VALUES(2,TO_DATE('2016-01-09','YYYY-MM-DD'),'22:50',4,2,97,3,6);
INSERT INTO KURSY VALUES(3,TO_DATE('2016-01-09','YYYY-MM-DD'),'23:00',17,2,11,29,7);
INSERT INTO KURSY VALUES(4,TO_DATE('2016-01-09','YYYY-MM-DD'),'14:15',2,3,1,38,0);
INSERT INTO KURSY VALUES(5,TO_DATE('2016-01-09','YYYY-MM-DD'),'18:10',3,2,24,32,5);
INSERT INTO KURSY VALUES(6,TO_DATE('2016-01-09','YYYY-MM-DD'),'02:20',4,3,23,87,0);
INSERT INTO KURSY VALUES(7,TO_DATE('2016-01-09','YYYY-MM-DD'),'10:12',5,2,42,64,14);
INSERT INTO KURSY VALUES(8,TO_DATE('2016-01-09','YYYY-MM-DD'),'15:45',4,2,70,30,10);
INSERT INTO KURSY VALUES(9,TO_DATE('2016-01-09','YYYY-MM-DD'),'23:30',6,3,14,39,0);
INSERT INTO KURSY VALUES(10,TO_DATE('2016-01-10','YYYY-MM-DD'),'09:00',7,2,36,75,13);
INSERT INTO KURSY VALUES(11,TO_DATE('2016-01-11','YYYY-MM-DD'),'17:35',8,2,88,89,2);
INSERT INTO KURSY VALUES(12,TO_DATE('2016-01-11','YYYY-MM-DD'),'20:40',10,2,11,29,11);
INSERT INTO KURSY VALUES(13,TO_DATE('2015-01-11','YYYY-MM-DD'),'21:32',11,2,37,81,4);
INSERT INTO KURSY VALUES(14,TO_DATE('2016-01-11','YYYY-MM-DD'),'16:36',14,2,30,50,6);
INSERT INTO KURSY VALUES(15,TO_DATE('2016-01-15','YYYY-MM-DD'),'11:34',12,2,22,99,5);
INSERT INTO KURSY VALUES(16,TO_DATE('2016-01-31','YYYY-MM-DD'),'07:30',13,3,66,38,0);
INSERT INTO KURSY VALUES(17,TO_DATE('2016-01-31','YYYY-MM-DD'),'22:50',14,3,152,137,0);
INSERT INTO KURSY VALUES(18,TO_DATE('2016-01-12','YYYY-MM-DD'),'23:00',1,2,145,107,11);
INSERT INTO KURSY VALUES(19,TO_DATE('2016-01-15','YYYY-MM-DD'),'14:11',8,2,90,101,4);
INSERT INTO KURSY VALUES(20,TO_DATE('2016-01-16','YYYY-MM-DD'),'10:50',9,2,52,49,3);
INSERT INTO KURSY VALUES(21,TO_DATE('2016-01-16','YYYY-MM-DD'),'23:00',12,2,47,34,2);
INSERT INTO KURSY VALUES(22,TO_DATE('2016-01-22','YYYY-MM-DD'),'17:35',8,1,14,131,0);
INSERT INTO KURSY VALUES(23,TO_DATE('2016-01-22','YYYY-MM-DD'),'10:20',17,1,27,80,0);
INSERT INTO KURSY VALUES(24,TO_DATE('2016-01-22','YYYY-MM-DD'),'22:55',12,1,56,100,0);
INSERT INTO KURSY VALUES(25,TO_DATE('2016-01-23','YYYY-MM-DD'),'22:55',2,1,120,123,0);
INSERT INTO KURSY VALUES(26,TO_DATE('2016-01-23','YYYY-MM-DD'),'22:55',8,3,35,125,0);
INSERT INTO KURSY VALUES(27,TO_DATE('2016-01-23','YYYY-MM-DD'),'22:55',13,1,42,8,0);

