<?php session_start(); 
	if(isset($_SESSION['zalogowany']))
	{
		if(empty($_POST['login']) AND !isset($_SESSION['kto']))
			require("check.php");
		if($_SESSION['zalogowany']!=1)
		{	
			require("check.php");
		}
	}
	else{require("check.php");}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="../../favicon.ico">

		<title>Moje konto</title>

		 <!-- Bootstrap core CSS -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">

		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="../css/offcanvas.css" rel="stylesheet">

		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="../../assets/js/ie-emulation-modes-warning.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>
		<nav class="navbar navbar-fixed-top navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<a href="index.php"><img class="navbar-brand" src="../img/logo_zagan_taxi.png" alt="Logo Firmy"/></a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<form method="post" class="navbar-form navbar-right" action="index.php">
						<div class="form-group"></div>
							<input type='hidden' name='wyloguj' value='TRUE'/>
						<button type="submit" class="btn btn-danger navbar-right"><span class="glyphicon glyphicon-log-out"></span> <?php echo $_SESSION['kto'] ?> - wyloguj się</button>
					</form>
				</div><!--/.navbar-collapse -->
			</div><!-- /.container -->
		</nav><!-- /.navbar -->

		<div class="container">
			<div class="row row-offcanvas row-offcanvas-right">
				<div class="col-xs-12 col-sm-18">
					<p class="pull-right visible-xs">
						<button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
					</p>
					<div class="jumbotron">
						<?php
							require("polacz.php");

							$login=$_SESSION['kto'];	

							if (empty($_POST['anuluj_kurs'])){}
								else
								{
									$ak_stid = oci_parse($conn, $_POST['anuluj_kurs'].$_POST['anuluj_kurs_kursu']);
									$r_ak = oci_execute($ak_stid);
								}
							print '
								<ul class="nav nav-pills" role="tablist">
									<form method="post" action="moje_dane.php">';
										print "<input type='hidden' name='login' value='$_SESSION[kto]'/>";
										print '	<button type="submit" class="btn btn-info navbar-right"><span class="glyphicon glyphicon-cog"></span> <?php echo $_POST["login"] ?> Moje dane</button>
										<a href="dodaj_kurs.php"  class="btn btn-success navbar-right" role="button"><span class="glyphicon glyphicon-plus-sign"></span> <?php echo $_POST["login"] ?> Dodaj kurs</a>
									</form>
								</ul>
								<br>';
							
							$query = "SELECT K.KURSYID, TO_CHAR(K.DATA,'DD-MM-YYYY'), K.GODZINA, S.STATUS, U.NAZWA AS SKAD, U2.NAZWA AS DOKAD, P.NR_BOCZNY
									  FROM POJAZDY P, KLIENCI KL, KURSY K, ULICE U, ULICE U2, STATUSY_KURSOW S
									  WHERE (K.KLIENCI_ID = KL.KLIENCI_ID AND K.STATUS_ID = S.STATUS_ID AND K.SKAD = U.ULICE_ID AND K.DOKAD = U2.ULICE_ID AND P.POJAZDY_ID = K.POJAZDY_ID) AND (KL.LOGIN = '$login')
									  ORDER BY KURSYID";
							$stid = oci_parse($conn, $query);
							ocidefinebyname($stid,"STATUS",$aktualnyStatus);
							ocidefinebyname($stid,"KURSYID",$aktualneIdKursu);
							$r = oci_execute($stid);
				
							$zlicz = "SELECT COUNT(*) FROM	POJAZDY P, KLIENCI KL, KURSY K, ULICE U, ULICE U2, STATUSY_KURSOW S
									  WHERE (K.KLIENCI_ID = KL.KLIENCI_ID AND K.STATUS_ID = S.STATUS_ID AND K.SKAD = U.ULICE_ID AND K.DOKAD = U2.ULICE_ID AND P.POJAZDY_ID = K.POJAZDY_ID) AND (KL.LOGIN = '$login')";
							$stid2 = oci_parse($conn, $zlicz);
							$r2 = oci_execute($stid2);
							
							while($row2 = oci_fetch_array($stid2, OCI_RETURN_NULLS + OCI_ASSOC)) 
							{
										print '<tr>';
										foreach ($row2 as $item) 
										{
											$ilewierszy=($item !==null ?  htmlentities($item, ENT_QUOTES) : '&nbsp');
										}

										print '</tr>';
							}
							if ($ilewierszy==0)
							{
								print '<div class="alert alert-danger">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											<center><strong>Nie znaleziono,</strong> ponieważ nie korzystałeś jeszcze z usług naszej firmy.
										</div>';
								print '<div class="alert alert-info">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											<center>Kliknij <a href="#" class="btn btn-success btn-xs" role="button"><span class="glyphicon glyphicon-plus-sign"></span> <?php echo $_POST["login"] ?> Dodaj kurs</a> aby zamówić taksówkę.
										</div>';
							}
							else
							{			
								print '<h2><center>MOJE KURSY:</h2>';
								print '<center><table class="table table-hover">';
									print '<thead>';
									print '<tr>';
										print '<th><center>#</th>';
										print '<th><center>Data</th>';
										print '<th><center>Godzina</th>';
										print '<th><center>Status kursu</th>';
										print '<th><center>Początek kursu</th>';
										print '<th><center>Koniec kursu</th>';	
										print '<th><center>Taksówka</th>';
										print '<th><center>Zarządzaj</th>';					
										print '</tr>';
										print '</thead>';
											
										while($row = oci_fetch_array($stid, OCI_RETURN_NULLS + OCI_ASSOC)) 
										{
											print '<tr>';
												foreach ($row as $item) 
												{
													print '<td><center>' .($item !==null ?  htmlentities($item, ENT_QUOTES) : '&nbsp').'</td>';
												}
												print '<form method="post">';											
															$anuluj_kurs="UPDATE KURSY SET STATUS_ID=3 WHERE KURSYID='$aktualneIdKursu'";												
															print "<input type='hidden' name='anuluj_kurs' value='$anuluj_kurs'>";
															print "<input type='hidden' name='anuluj_kurs_kursu' value='$aktualneIdKursu'>";
															print "<input type='hidden' name='login' value='$login'>";												
															if($aktualnyStatus=='Oczekujący')
																print '<td><center><button type="submit" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove	"></span> Anuluj kurs</button></td>';
												print '</form>';
												if($aktualnyStatus!='Oczekujący')
													print '<td><center><button class="btn btn-danger btn-xs disabled"><span class="glyphicon glyphicon-remove	"></span> Anuluj kurs</button></td>';
											print '</tr>';
										}
								print '</table>';
							}
							oci_close($conn);
						?>
					</div>
				</div><!--/.col-xs-12.col-sm-9-->
			</div><!--/row-->
			<hr>
			<footer>
				<p>Adam Adamczyk 321 IDZ</p>
			</footer>
		</div><!--/.container-->


		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
		<script src="../js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
		<script src="../css/offcanvas.js"></script>
	</body>
</html>
