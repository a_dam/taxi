<?php session_start(); 
	if(isset($_SESSION['zalogowany']))
	{
		if($_SESSION['zalogowany']!=1)
		{	
			require("check.php");
		}
	}
	else{require("check.php");}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="../../favicon.ico">

		<title>Moje dane</title>

		 <!-- Bootstrap core CSS -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">

		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="../css/offcanvas.css" rel="stylesheet">

		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="../../assets/js/ie-emulation-modes-warning.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>
		<nav class="navbar navbar-fixed-top navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<a href="index.php"><img class="navbar-brand" src="../img/logo_zagan_taxi.png" alt="Logo Firmy"/></a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<form method="post" class="navbar-form navbar-right" action="index.php">
						<div class="form-group"></div>
							<input type='hidden' name='wyloguj' value='TRUE'/>
						<button type="submit" class="btn btn-danger navbar-right"><span class="glyphicon glyphicon-log-out"></span> <?php echo $_POST['login'] ?> - wyloguj się</button>
					</form>
				</div><!--/.navbar-collapse -->
			</div><!-- /.container -->
		</nav><!-- /.navbar -->

		<div class="container">
			<div class="row row-offcanvas row-offcanvas-right">
				<div class="col-xs-12 col-sm-18">
					<p class="pull-right visible-xs">
						<button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
					</p>
					<div class="jumbotron">
						<?php
							require("polacz.php");
								
							$login=$_POST['login'];
							$zalogowanyjako=$_POST['login'];
						
							$query = "SELECT * FROM KLIENCI WHERE LOGIN = '$login'";
														
							$stid = oci_parse($conn, $query);
							ocidefinebyname($stid,"IMIE",$imie);
							ocidefinebyname($stid,"NAZWISKO",$nazwisko);
							ocidefinebyname($stid,"NR_TELEFONU",$nr_telefonu);
							ocidefinebyname($stid,"E_MAIL",$e_mail);
							ocidefinebyname($stid,"HASLO",$haslo);					
							$r = oci_execute($stid);
							$edytowany_login=$_POST['login'];
							while($row = oci_fetch_array($stid, OCI_RETURN_NULLS + OCI_ASSOC))
							{
									$edytowany_email=$e_mail;
									$edytowany_nr_telefonu=$nr_telefonu;
							}
								
							if (empty($_POST["imie"]) || empty($_POST["nazwisko"]) || empty($_POST["nr_telefonu"]) || empty($_POST["email"]) || empty($_POST["zmieniany_login"]) || empty($_POST["haslo"]))
							{
									print '<div class="alert alert-info">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<center>Zmień wartość pól w celu zmiany danych.
											</div>';
						
									print '<div class="alert alert-warning	">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<center>Kliknij wroć do panelu jeśli nie chcesz zmieniać swoich danych.
											</div>';
						
									print 	"<center><form class='navbar-form'  method='post'>
													<div class='form-group'>   
														<b>Imię:</b><br>								
														<input type='text' placeholder='Imię' name='imie' value='$imie' class='form-control'>
													</div>
													<br>
													<div class='form-group'> 
														<br><b>Nazwisko:</b><br>									
														<input type='text' placeholder='Nazwisko' name='nazwisko' value='$nazwisko' class='form-control'>
													</div>
													<br>
													<div class='form-group'>      
														<br><b>Nr telefonu:</b><br>									
														<input type='text' placeholder='Nr telefonu' pattern='^\d{9}$' title='Format: 9 cyfr, np. 123456789' name='nr_telefonu' value='$nr_telefonu' class='form-control'>
													</div>
													<br>
													<div class='form-group'>        
														<br><b>Adres e-mail:</b><br>									
														<input type='text' placeholder='Adres e-mail' name='email' value='$e_mail' class='form-control'>
													</div>
													<br>
													<div class='form-group'>     
														<br><b>Login:</b><br>									
														<input type='text' placeholder='Login' name='zmieniany_login' value='$login' class='form-control'>
													</div>
													<br>
													<div class='form-group'> 
														<br><b>Hasło:</b><br>									
														<input type='password' placeholder='Hasło' name='haslo' value='$haslo' class='form-control'>
													</div>
												
													<br><br>
													<input type='hidden' name='login' value='$login'/>
													<input type='hidden' name='e_login' value='$edytowany_login'>
													<input type='hidden' name='e_email' value='$edytowany_email'>
													<input type='hidden' name='e_nr_telefonu' value='$edytowany_nr_telefonu'>
													<button type='submit' class='btn btn-info'><span class='glyphicon glyphicon-edit'></span> Edytuj</button>		
											</form>
											<form method='post' action='panel_klienta.php'>
												<input type='hidden' name='login' value='$zalogowanyjako'/>
												<button type='submit' class='btn btn-danger'><span class='glyphicon glyphicon-remove-sign'></span> Anuluj</button>
											</form>";											
							}
							else
							{
								$sprawdz = "SELECT E_MAIL, LOGIN, NR_TELEFONU FROM KLIENCI";
								$stid = oci_parse($conn, $sprawdz);
								ocidefinebyname($stid,"E_MAIL",$sprawdzanyE_mail);
								ocidefinebyname($stid,"LOGIN",$sprawdzanyLogin);
								ocidefinebyname($stid,"NR_TELEFONU",$sprawdzanyNR_TELEFON);
								$r = oci_execute($stid);
								$istniejacyEmail=0;
								$istniejacyTelefon=0;
								$istniejacyLogin=0;
								while($row = oci_fetch_array($stid, OCI_RETURN_NULLS + OCI_ASSOC))
								{
									if($_POST['email']==$sprawdzanyE_mail)
									$istniejacyEmail=1;

									if($_POST['nr_telefonu']==$sprawdzanyNR_TELEFON)
										$istniejacyTelefon=1;
											
									if($_POST['zmieniany_login']==$sprawdzanyLogin)
										$istniejacyLogin=1;
								}
								
								
								if($_POST['email']==$edytowany_email) $istniejacyEmail=0;
								if($_POST['zmieniany_login']==$edytowany_login) $istniejacyLogin=0;
								if($_POST['nr_telefonu']==$edytowany_nr_telefonu) $istniejacyTelefon=0;
							
								if($istniejacyEmail==0 AND $istniejacyTelefon==0 AND $istniejacyLogin==0)
								{
									header("refresh:3;url=panel_klienta.php");
									/*print	"<form method='post' action='panel_klienta.php'>
									print	<input type='hidden' name='login' value='$login'/>
											</form>";
									print	'<script language="javascript" type="text/javascript">
														document.forms[0].submit();
												</script>';*/
									
									print  '<div class="alert alert-success">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<center><strong>Edytowano!</strong> Za chwilkę nastąpi przekierowanie do panelu użytkownika.
											</div>';
									$zmien_dane="UPDATE KLIENCI SET IMIE = '$_POST[imie]', NAZWISKO = '$_POST[nazwisko]', NR_TELEFONU = $_POST[nr_telefonu], E_MAIL = '$_POST[email]', LOGIN = '$_POST[zmieniany_login]', HASLO = '$_POST[haslo]' WHERE LOGIN = '$_SESSION[kto]'";
									//print $zmien_dane;
									$zd_stid = oci_parse($conn, $zmien_dane);
									$r_zd = oci_execute($zd_stid);	
									$_SESSION['kto']=$_POST['zmieniany_login'];
								}
								else
								{
									if($istniejacyEmail==1)
									{
										print '<div class="alert alert-danger">
													<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
													<center><strong>Duplikat!</strong>  Podany adres e-mail istnieje już w systemie.
												</div>';
									}
												
									if($istniejacyTelefon==1)
									{
										print '<div class="alert alert-danger">
													<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
													<center><strong>Duplikat!</strong>  Użytkownik posiadający podany numer telefonu istnieje już w systemie.
												</div>';}
												
									if($istniejacyLogin==1)
									{	
										print '<div class="alert alert-danger">
													<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
													<center><strong>Duplikat!</strong>  Podany login jest już zajęty.
												</div>';
									}
								
									print 	"<center><form class='navbar-form'  method='post'>
												<div class='form-group'>   
													<b>Imię:</b><br>								
													<input type='text' placeholder='Imię' name='imie' value='$_POST[imie]' class='form-control'>
												</div>
												<br>
												<div class='form-group'> 
													<br><b>Nazwisko:</b><br>									
													<input type='text' placeholder='Nazwisko' name='nazwisko' value='$_POST[nazwisko]' class='form-control'>
												</div>
												<br>
												<div class='form-group'>      
													<br><b>Nr telefonu:</b><br>									
													<input type='text' placeholder='Nr telefonu' pattern='^\d{9}$' title='Format: 9 cyfr, np. 123456789' name='nr_telefonu' value='$_POST[nr_telefonu]' class='form-control'>
												</div>
												<br>
												<div class='form-group'>        
													<br><b>Adres e-mail:</b><br>									
													<input type='text' placeholder='Adres e-mail' name='email' value='$_POST[email]' class='form-control'>
												</div>
												<br>
												<div class='form-group'>     
													<br><b>Login:</b><br>									
													<input type='text' placeholder='Login' name='zmieniany_login' value='$_POST[zmieniany_login]' class='form-control'>
												</div>
												<br>
												<div class='form-group'> 
													<br><b>Hasło:</b><br>									
													<input type='password' placeholder='Hasło' name='haslo' value='$_POST[haslo]' class='form-control'>
												</div>
												<br><br>
												<button type='submit' class='btn btn-info'><span class='glyphicon glyphicon-edit'></span> Edytuj</button>
											<input type='hidden' name='login' value='$zalogowanyjako'/>
										</form>
										<form method='post' action='panel_klienta.php'>
											<input type='hidden' name='login' value='$zalogowanyjako'/>
											<button type='submit' class='btn btn-danger'><span class='glyphicon glyphicon-remove-sign'></span> Anuluj</button>
										</form>";
								}
							}
						?>
					</div>
				</div><!--/.col-xs-12.col-sm-9-->
			</div><!--/row-->	
			<hr>	
			<footer>
				<p>Adam Adamczyk 321 IDZ</p>
			</footer>
		</div><!--/.container-->
		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
		<script src="../js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
		<script src="../css/offcanvas.js"></script>
	</body>
</html>
