<?php session_start(); 
	if(empty($_POST['e-mail']) OR empty($_POST['e-mail']))
	{
		header('Location: index.php');
		exit;
	}
	if(isset($_SESSION['zalogowany']))
	{
		if($_SESSION['zalogowany']==0)
		{	
				header('Location: panel_dyspozytorki.php');
				exit;
		}
		if($_SESSION['zalogowany']==1)
		{	
				header('Location: index.php');
				exit;
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="../../favicon.ico">

		<title>Logowanie</title>

		<!-- Bootstrap core CSS -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">

		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="../css/offcanvas.css" rel="stylesheet">

		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="../../assets/js/ie-emulation-modes-warning.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>
		<nav class="navbar navbar-fixed-top navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<a href="index.php"><img class="navbar-brand" src="../img/logo_zagan_taxi.png" alt="Logo Firmy"/></a>
				</div>
			</div><!-- /.container -->
		</nav><!-- /.navbar -->

		<div class="container">
			<div class="row row-offcanvas row-offcanvas-right">
				<div class="col-xs-12 col-sm-18">
					<p class="pull-right visible-xs">
						<button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
					</p>
						<div class="jumbotron">
							<?php
								require("polacz.php");
						 
								$query = "SELECT KLIENCI_ID, E_MAIL, LOGIN, HASLO FROM KLIENCI";
																
								$stid = oci_parse($conn, $query);
								ocidefinebyname($stid,"KLIENCI_ID",$klient_id);
								ocidefinebyname($stid,"E_MAIL",$email);
								ocidefinebyname($stid,"LOGIN",$login);
								ocidefinebyname($stid,"HASLO",$haslo);
								$r = oci_execute($stid);
								
								while($row = oci_fetch_array($stid, OCI_RETURN_NULLS + OCI_ASSOC))
								{
									print '<tr>';
									if ($_POST['e-mail']==$email AND $_POST['haslo']==$haslo)
									{
										$email=$_POST['e-mail'];
										$haslo=$_POST['haslo'];
										if($email=='dyspozytorka@taxi.pl')
										{
											$_SESSION['zalogowany'] = 0;
											print	"<form method='post' action='panel_dyspozytorki.php'>";
										}
										else
										$_SESSION['zalogowany'] = 1;
										$_SESSION['kto'] = $login;
										$_SESSION['kto_id']=$klient_id;
										print	"<form method='post' action='panel_klienta.php'>";
										print		"<input type='hidden' name='e-mail' value='$email'/>
													<input type='hidden' name='haslo' value='$haslo'/>
													<input type='hidden' name='login' value='$login'/>
												</form>";
										print	'<script language="javascript" type="text/javascript">
														document.forms[0].submit();
												</script>';
									}
										print '</tr>';
								}
								print '</table>';
										
								if (empty($_POST['email']) OR empty($_POST['haslo']))
								{
									print 	'<div class="alert alert-danger">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<center><strong>Niezalogowano!</strong> Podano błędny adres e-mail lub hasło. Wprowadź poprawne dane.
											</div>';
									print 	'<center><form class="navbar-form" action="logowanie.php" method="post">
												<div class="form-group">             
													<input type="email" placeholder="Adres e-mail" name="e-mail" value="" class="form-control">
												</div>
												<div class="form-group">
													<input type="password" placeholder="Hasło" name="haslo" class="form-control">
												</div>
												<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-log-in"></span> Zaloguj się</button>
											</form>';
								}
							?>
						</div>
			  
				</div><!--/.col-xs-12.col-sm-9-->

			
			</div><!--/row-->

			<hr>

			<footer>
				<p>Adam Adamczyk 321 IDZ</p>
			</footer>

		</div><!--/.container-->


		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
		<script src="../js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
		<script src="../css/offcanvas.js"></script>
	</body>
</html>
