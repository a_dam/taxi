<?php session_start(); 
	if(isset($_SESSION['zalogowany']))
	{
		if($_SESSION['zalogowany']!=0)
		{	
			require("check.php");
		}
	}
	else{require("check.php");}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="../../favicon.ico">

		<title>Zakończ kurs</title>
		<!-- Bootstrap core CSS -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">

		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="../css/offcanvas.css" rel="stylesheet">

		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="../../assets/js/ie-emulation-modes-warning.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
	</head>

	<body>
		<nav class="navbar navbar-fixed-top navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<a href="index.php"><img class="navbar-brand" src="../img/logo_zagan_taxi.png" alt="Logo Firmy"/></a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<form class="navbar-form navbar-right">
						<a href="index.php" class="btn btn-danger navbar-right" role="button"><span class="glyphicon glyphicon-log-out"></span> <?php echo $_SESSION['kto']?> - wyloguj się</a>
					</form>
				</div><!--/.navbar-collapse -->
			</div><!-- /.container -->
		</nav><!-- /.navbar -->

		<div class="container">
			<div class="row row-offcanvas row-offcanvas-right">
				<div class="col-xs-12 col-sm-18">
					<p class="pull-right visible-xs">
						<button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
					</p>
					<div class="jumbotron">
						<?php
							require("polacz.php");
							$query = "SELECT MARKA, MODEL, NR_BOCZNY FROM POJAZDY WHERE POJAZDY_ID>0 ORDER BY POJAZDY_ID ASC";
							
							$stid = oci_parse($conn, $query);
							ocidefinebyname($stid,"MARKA",$marka);
							ocidefinebyname($stid,"MODEL",$model);
							ocidefinebyname($stid,"NR_BOCZNY",$nr_boczny);
							$r = oci_execute($stid);
											
							if (empty($_POST["taksowka"]))
							{		
								print "<center><form class='navbar-form'  method='post'>
											<div class='form-group'>
											<br><b><label for='taksowka'>Wybierz taksówkę realizującą kurs:</label></b><br>
												<select name='taksowka' class='form-control'>";
													$licznik=1;
													while($row = oci_fetch_array($stid, OCI_RETURN_NULLS + OCI_ASSOC))
													{
														print"<option value=$licznik>$marka $model - numer boczny: 	$nr_boczny</option>";
														$licznik++;
													}
												print "</select><br>
											</div>	
											<br><br>
											<input type='hidden' name='kurs_id' value='$_POST[zmien_status_kursu]'>
											<input type='hidden' name='ktora_zakladka' value='Kursy'>
											<button type='submit' class='btn btn-info'><span class='glyphicon glyphicon-plus-sign'></span> Wybierz</button>
											<a href='panel_dyspozytorki.php' class='btn btn-warning' role='button'><span class='glyphicon glyphicon-remove-sign'></span> Anuluj</a>
										</form>";
								
							}
							else
							{
								header("refresh:3;url=panel_dyspozytorki.php");
								print  '<div class="alert alert-success">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											<center><strong>Wybrano taksówkę i zakończono kurs!</strong> Za chwilkę nastąpi przekierowanie do panelu.
										</div>';
								$wybierz_taksowke="UPDATE KURSY SET STATUS_ID=2, POJAZDY_ID=$_POST[taksowka] WHERE KURSYID=$_POST[kurs_id]";
								print $wybierz_taksowke;
								$wt_stid = oci_parse($conn, $wybierz_taksowke);
								$r_wt = oci_execute($wt_stid);			
							}	
						?>
					</div>
				</div><!--/.col-xs-12.col-sm-9-->
			</div><!--/row-->
			<hr>
			<footer>
				<p>Adam Adamczyk 321 IDZ</p>
			</footer>
		</div><!--/.container-->

		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
		<script src="../js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
		<script src="../css/offcanvas.js"></script>
	</body>
</html>
	