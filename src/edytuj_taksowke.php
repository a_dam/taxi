<?php session_start(); 
	if(isset($_SESSION['zalogowany']))
	{
		if($_SESSION['zalogowany']!=0)
		{	
			require("check.php");
		}
	}
	else{require("check.php");}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="../../favicon.ico">

		<title>Edytuj taksówkę</title>
		<!-- Bootstrap core CSS -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">

		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="../css/offcanvas.css" rel="stylesheet">

		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="../../assets/js/ie-emulation-modes-warning.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>
		<nav class="navbar navbar-fixed-top navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<a href="index.php"><img class="navbar-brand" src="../img/logo_zagan_taxi.png" alt="Logo Firmy"/></a>
				
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<form class="navbar-form navbar-right">
						<a href="index.php" class="btn btn-danger navbar-right" role="button"><span class="glyphicon glyphicon-log-out"></span> dyspozytorka - wyloguj się</a>
					</form>
				</div><!--/.navbar-collapse -->
			</div><!-- /.container -->
		</nav><!-- /.navbar -->

		<div class="container">
			<div class="row row-offcanvas row-offcanvas-right">
				<div class="col-xs-12 col-sm-18">
					<p class="pull-right visible-xs">
						<button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
					</p>
					<div class="jumbotron">
						<?php
							require("polacz.php");
						
							$edytowany_nr_rejestracyjny=$_POST['nr_rejestracyjny'];
							$edytowany_nr_boczny=$_POST['nr_boczny'];
										
							$query = "SELECT Kategoria FROM Kategorie_pojazdow";
							$query2 = "SELECT Kolor FROM Kolory_pojazdow order by kolor_id asc";
							$query3 = "SELECT POJAZDY_ID FROM POJAZDY order by POJAZDY_id asc";	
							$query4 = "SELECT Marka, Model, Nr_rejestracyjny, nr_boczny, liczba_miejsc, kategoria_id, kolor_id, kierowca_imie, kierowca_nazwisko FROM POJAZDY WHERE nr_rejestracyjny = '$edytowany_nr_rejestracyjny'";						
							
							$stid = oci_parse($conn, $query);
							ocidefinebyname($stid,"KATEGORIA",$kategoria);
							$r = oci_execute($stid);
							
							$stid2 = oci_parse($conn, $query2);
							ocidefinebyname($stid2,"KOLOR",$kolor);
							$r2 = oci_execute($stid2);
							
							$stid3 = oci_parse($conn, $query3);
							ocidefinebyname($stid3,"POJAZDY_ID",$pojazd_id);
							$r3 = oci_execute($stid3);
									
							$stid4 = oci_parse($conn, $query4);
							ocidefinebyname($stid4,"MARKA",$marka);
							ocidefinebyname($stid4,"MODEL",$model);
							ocidefinebyname($stid4,"NR_REJESTRACYJNY",$n_nr_rejestracyjny);
							ocidefinebyname($stid4,"NR_BOCZNY",$nr_boczny_baza);
							ocidefinebyname($stid4,"LICZBA_MIEJSC",$liczba_miejsc);
							ocidefinebyname($stid4,"KATEGORIA_ID",$kategoria_id);
							ocidefinebyname($stid4,"KOLOR_ID",$kolor_id);
							ocidefinebyname($stid4,"KIEROWCA_IMIE",$kierowca_imie);
							ocidefinebyname($stid4,"KIEROWCA_NAZWISKO",$kierowca_nazwisko);
							$r4 = oci_execute($stid4);		
									
							while($row = oci_fetch_array($stid4, OCI_RETURN_NULLS + OCI_ASSOC))		
							while($row = oci_fetch_array($stid3, OCI_RETURN_NULLS + OCI_ASSOC))
							{
								$p_id=$pojazd_id;
							}
							
							if (empty($_POST["marka"]) || empty($_POST["model"])  || empty($_POST["nr_boczny"]) || empty($_POST["l_miejsc"]) || empty($_POST["imie_kierowcy"]) || empty($_POST["nazwisko_kierowcy"]) )
							{
								print 	"<center><form class='navbar-form'  method='post'>
											<div class='form-group'>     
												<b>Marka:</b><br>
												<input type='text' placeholder='Marka' name='marka' value='$marka' class='form-control'>
											</div>
											<br>
											<div class='form-group'>   
											<br><b>Model:</b><br>									
												<input type='text' placeholder='Model' name='model' value='$model' class='form-control'>
											</div>
											<br>
											<div class='form-group'>    
												<br><b>Nr rejestracyjny:</b><br>								
												<input type='text' placeholder='Nr rejestracyjny' pattern='^[A-Z0-9]{4,8}' title='Maksymalnie 8 znaków, tylko duże litery i cyfry, np. FZG12345' name='nowy_nr_rejestracyjny' value='$n_nr_rejestracyjny' class='form-control'>
											</div>
											<br>
											<div class='form-group'> 
												<br><b>Nr boczny:</b><br>									
												<input type='text' placeholder='Nr boczny' pattern='^\d{4}$' title='Format: 4 cyfry, np. 0000' name='nr_boczny_form' value='$nr_boczny_baza' class='form-control'>
											</div>
											<br>
											<div class='form-group'>     
												<br><b>Liczba miejsc:</b><br>									
												<input type='text' placeholder='Liczba miejsc' name='l_miejsc' value='$liczba_miejsc' class='form-control'>
											</div>
											<br>
											<div class='form-group'>             
													 
													<br>  <b><label for='kategoria_sel'>Kategoria:</label></b>
																						<br>	
													  <select name='kategoria_sel' class='form-control'>";
														$licznik=1;
														while($row = oci_fetch_array($stid, OCI_RETURN_NULLS + OCI_ASSOC))
														{
														if($licznik==$kategoria_id)
																print"<option selected='selected' value='$licznik'>$kategoria</option>";
															else	
																print"<option value=$licznik>$kategoria</option>";			
															$licznik++;
														}
													  print "</select>
													
											</div>
											<br>
											<div class='form-group'>
												<br>  <b><label for='kolor_sel'>Kolor:</label></b><br>
												<select class='form-control' name='kolor_sel'>";
													$licznik=1;
													while($row = oci_fetch_array($stid2, OCI_RETURN_NULLS + OCI_ASSOC))
													{
														if($licznik==$kolor_id)
															print"<option selected='selected' value='$licznik'>$kolor</option>";
														else	
															print"<option value=$licznik>$kolor</option>";			
														$licznik++;
													}
												print "</select>											
											</div><br>
											<div class='form-group'>       
												<br><b>Imię kierowcy:</b><br>									
												<input type='text' placeholder='Imię kierowcy' name='imie_kierowcy' value='$kierowca_imie' class='form-control'>
											</div>
											<br>
											<div class='form-group'>
												<br><b>Nazwisko kierowcy:</b><br>	
												<input type='text' placeholder='Nazwisko kierowcy' name='nazwisko_kierowcy' value='$kierowca_nazwisko' class='form-control'>
											</div>
											<br><br>
												<input type='hidden' name='nr_rejestracyjny' value='$edytowany_nr_rejestracyjny'>
												<input type='hidden' name='nr_boczny' value='$edytowany_nr_boczny'>
													<input type='hidden' name='ktora_zakladka' value='Taksowki'>
											<button type='submit' class='btn btn-info'><span class='glyphicon glyphicon-edit'></span> Edytuj</button>
											<a href='panel_dyspozytorki.php' class='btn btn-warning' role='button'><span class='glyphicon glyphicon-remove-sign'></span> Anuluj</a>

										</form>";
							}
							else
							{
								$sprawdz = "SELECT NR_REJESTRACYJNY, NR_BOCZNY FROM POJAZDY";
								$stid5 = oci_parse($conn, $sprawdz);
								ocidefinebyname($stid5,"NR_REJESTRACYJNY",$sprawdzanyNrRej);
								ocidefinebyname($stid5,"NR_BOCZNY",$sprawdzanyNrB);
								$r = oci_execute($stid5);
								$istniejacyNrRej=0;
								$istniejacyNrB=0;
								
								while($row = oci_fetch_array($stid5, OCI_RETURN_NULLS + OCI_ASSOC))
								{
									if($_POST['nowy_nr_rejestracyjny']==$sprawdzanyNrRej)
										$istniejacyNrRej=1;

									if($_POST['nr_boczny_form']==$sprawdzanyNrB)
										$istniejacyNrB=1;
								}
								
								if($_POST['nowy_nr_rejestracyjny']==$edytowany_nr_rejestracyjny) $istniejacyNrRej=0;
								if($_POST['nr_boczny_form']==$edytowany_nr_boczny) $istniejacyNrB=0;
							
								if($istniejacyNrRej==0 AND $istniejacyNrB==0)
								{
									header("refresh:3;url=panel_dyspozytorki.php");
									print  '<div class="alert alert-success">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<center><strong>Edytowano taksówkę!</strong> Za chwilkę nastąpi przekierowanie do panelu.
											</div>';
									$edytuj_taksowke="UPDATE POJAZDY SET MARKA='$_POST[marka]', MODEL='$_POST[model]', NR_REJESTRACYJNY='$_POST[nowy_nr_rejestracyjny]', NR_BOCZNY='$_POST[nr_boczny_form]', LICZBA_MIEJSC=$_POST[l_miejsc], KATEGORIA_ID=$_POST[kategoria_sel], KOLOR_ID=$_POST[kolor_sel], KIEROWCA_IMIE='$_POST[imie_kierowcy]', KIEROWCA_NAZWISKO='$_POST[nazwisko_kierowcy]' WHERE NR_REJESTRACYJNY='$edytowany_nr_rejestracyjny'";
									$et_stid = oci_parse($conn, $edytuj_taksowke);
									$r_ek = oci_execute($et_stid);			
									
								}
								else	
								{
									if($istniejacyNrB==1)
									{
										print '<div class="alert alert-danger">
													<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
													<center><strong>Duplikat!</strong>  Taksówka o podanym numerze bocznym istnieje już w systemie.
												</div>';
									}
														
									if($istniejacyNrRej==1)
									{
										print '<div class="alert alert-danger">
													<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
													<center><strong>Duplikat!</strong> Samochód o podanym numerze rejestracyjnym istnieje już w systemie.
												</div>';
									}
									
									print 	"<center><form class='navbar-form'  method='post'>
												<div class='form-group'>      
													<br><b>Marka:</b><br>									
													<input type='text' placeholder='Marka' name='marka' value='$_POST[marka]' class='form-control'>
												</div>
												<br>
												<div class='form-group'>    
												<br><b>Model:</b><br>								
													<input type='text' placeholder='Model' name='model' value='$_POST[model]' class='form-control'>
												</div>
												<br>
												<div class='form-group'>      
													<br><b>Nr rejestracyjny:</b><br>								
													<input type='text' placeholder='Nr rejestracyjny' name='nowy_nr_rejestracyjny' title='Maksymalnie 8 znaków, tylko duże litery i cyfry, np. FZG12345' value='$_POST[nowy_nr_rejestracyjny]' class='form-control'>
												</div>
												<br>
												<div class='form-group'>   
													<br><b>Nr boczny:</b><br>								
													<input type='text' placeholder='Nr boczny' title='Format: 4 cyfry, np. 0000' name='nr_boczny_form' value='$_POST[nr_boczny_form]' class='form-control'>
												</div>
												<br>
												<div class='form-group'>        
												<br><b>Liczba miejsc:</b><br>								
													<input type='text' placeholder='Liczba miejsc' name='l_miejsc' value='$_POST[l_miejsc]' class='form-control'>
												</div>
												<br>
												<div class='form-group'>             
													
														  <br><b><label for='kategoria_sel'>Kategoria:</label></b><br>
														  <select class='form-control' name='kategoria_sel'>";
																$licznik=1;
																while($row = oci_fetch_array($stid, OCI_RETURN_NULLS + OCI_ASSOC))
																{
																	if($licznik==$_POST[kategoria_sel])
																		print"<option selected='selected' value='$licznik'>$kategoria</option>";
																	else	
																		print"<option value='licznik'>$kategoria</option>";
																	$licznik++;
																}
														  print "</select>
														</div>														
												<br>
												<div class='form-group'>													
														<br>  <label for='kolor_sel'>Kolor:</label><br>
														  <select name='kolor_sel' class='form-control'>";
																$licznik=1;
																while($row = oci_fetch_array($stid2, OCI_RETURN_NULLS + OCI_ASSOC))
																{
																	if($licznik==$_POST[kolor_sel])
																		print"<option selected='selected' value='$licznik'>$kolor</option>";
																	else
																		print"<option value='$licznik'>$kolor</option>";
																	$licznik++;
																}
														  print "</select>
												</div><br>
												<div class='form-group'>     
												<br><b>Imię kierowcy:</b><br>										
													<input type='text' placeholder='Imię kierowcy' name='imie_kierowcy' value='$_POST[imie_kierowcy]' class='form-control'>
												</div>
												<br>
												<div class='form-group'>
													<br><b>Nazwisko kierowcy:</b><br>		
													<input type='text' placeholder='Nazwisko kierowcy' name='nazwisko_kierowcy' value='$_POST[nazwisko_kierowcy]' class='form-control'>
												</div>
												<br><br>
												<input type='hidden' name='nr_rejestracyjny' value='$edytowany_nr_rejestracyjny'>
												
												<input type='hidden' name='nr_boczny' value='$edytowany_nr_boczny'>
												<button type='submit' class='btn btn-info'><span class='glyphicon glyphicon-edit'></span> Edytuj</button>
												<a href='panel_dyspozytorki.php' class='btn btn-warning' role='button'><span class='glyphicon glyphicon-remove-sign'></span> Anuluj</a>
								</form>";
							
								}
									
								
					
							}
							
						?>
					</div>
				</div><!--/.col-xs-12.col-sm-9-->			
			</div><!--/row-->
			<hr>
			<footer>
				<p>Adam Adamczyk 321 IDZ</p>
			</footer>
		</div><!--/.container-->
		
		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
		<script src="../js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
		<script src="../css/offcanvas.js"></script>
	</body>
</html>
