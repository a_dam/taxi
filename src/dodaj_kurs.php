<?php session_start(); 
	if(isset($_SESSION['zalogowany']))
	{
		if(empty($_POST['login']) AND !isset($_SESSION['kto']))
			require("check.php");
		if($_SESSION['zalogowany']!=1)
		{	
			require("check.php");
		}
	}
	else{require("check.php");}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="../../favicon.ico">

		<title>Dodaj kurs</title>
		<!-- Bootstrap core CSS -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">

		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="../css/offcanvas.css" rel="stylesheet">

		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="../../assets/js/ie-emulation-modes-warning.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
	</head>

	<body>
		<nav class="navbar navbar-fixed-top navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<a href="index.php"><img class="navbar-brand" src="../img/logo_zagan_taxi.png" alt="Logo Firmy"/></a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<form class="navbar-form navbar-right">
						<a href="index.php" class="btn btn-danger navbar-right" role="button"><span class="glyphicon glyphicon-log-out"></span> <?php echo $_SESSION['kto']?> - wyloguj się</a>
					</form>
				</div><!--/.navbar-collapse -->
			</div><!-- /.container -->
		</nav><!-- /.navbar -->

		<div class="container">
			<div class="row row-offcanvas row-offcanvas-right">
				<div class="col-xs-12 col-sm-18">
					<p class="pull-right visible-xs">
						<button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
					</p>
					<div class="jumbotron">
						<?php
							require("polacz.php");
							$query = "SELECT NAZWA FROM ULICE";
							$query2 = "SELECT KURSYID FROM KURSY order by KURSYID ASC";
											
							
							$stid = oci_parse($conn, $query);
							ocidefinebyname($stid,"NAZWA",$ulica);
							$r = oci_execute($stid);
							
							
							$stid3 = oci_parse($conn, $query);
							ocidefinebyname($stid3,"NAZWA",$ulica2);
							$r = oci_execute($stid3);
							
							
							$stid2 = oci_parse($conn, $query2);
							ocidefinebyname($stid2,"KURSYID",$kurs_id);
							$r2 = oci_execute($stid2);
							
							
									
							while($row = oci_fetch_array($stid2, OCI_RETURN_NULLS + OCI_ASSOC))
							{
								$k_id=$kurs_id;
							}
							$nowy_kurs_id=$k_id+1;
							
							if (empty($_POST["data"]) || empty($_POST["godzina"])) 
							{
								print '<div class="alert alert-info">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											<center>Aby dodać kurs wypełnij poniższy formularz i kliknij "Dodaj kurs".
										</div>';
					
								print '<div class="alert alert-warning	">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											<center>Aby prawidłowo dodać kurs, konieczne jest podanie wszystkich danych.
										</div>';
					
								print "<center><form class='navbar-form'  method='post'>
											<div class='form-group'>  
												<br><b>Data:</b><br>";
												$dzisiejsza_data=date('Y-m-d');
											print "<input type='date'  min='$dzisiejsza_data' max='2016-06-30' name='data' value='' class='form-control'>
											</div>
											<br>
											<div class='form-group'>  
												<br><b>Godzina:</b><br>";																								
											print "	<input type='time' placeholder='Model'  name='godzina' value='' class='form-control'>
											</div>
											<br>        
												<div class='form-group'>
												<br><b><label for='skad_sel'>Skąd:</label></b><br>
													<select name='skad_sel' class='form-control'>";
														$licznik=1;
														while($row = oci_fetch_array($stid, OCI_RETURN_NULLS + OCI_ASSOC))
														{
															print"<option value=$licznik>$ulica</option>";
															$licznik++;
														}
													print "</select>
												</div>	
											
											<br>
											<div class='form-group'>
												
													<br><b><label for='dokad_sel'>Dokąd:</label></b><br>
													 <select class='form-control' name='dokad_sel'>";
														$licznik=1;
														while($row = oci_fetch_array($stid3, OCI_RETURN_NULLS + OCI_ASSOC))
														{
															print"<option value=$licznik>$ulica2</option>";
															$licznik++;
														}
													 print "</select>
												
											</div><br><br>
											
											<button type='submit' class='btn btn-info'><span class='glyphicon glyphicon-plus-sign'></span> Dodaj</button>
											<a href='panel_klienta.php' class='btn btn-warning' role='button'><span class='glyphicon glyphicon-remove-sign'></span> Anuluj</a>
									</form>";
								
							}
							else
							{
								header("refresh:3;url=panel_klienta.php");
								print  '<div class="alert alert-success">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											<center><strong>Dodano kurs!</strong> Za chwilkę nastąpi przekierowanie do panelu.
										</div>';
								$nowy_kurs="INSERT INTO KURSY VALUES ($nowy_kurs_id,TO_DATE('$_POST[data]','YYYY-MM-DD'),'$_POST[godzina]',$_SESSION[kto_id],1,$_POST[skad_sel],$_POST[dokad_sel],0)";
								$nk_stid = oci_parse($conn, $nowy_kurs);
								$r_nk = oci_execute($nk_stid);			
							}	
						?>
					</div>
				</div><!--/.col-xs-12.col-sm-9-->
			</div><!--/row-->
			<hr>
			<footer>
				<p>Adam Adamczyk 321 IDZ</p>
			</footer>
		</div><!--/.container-->

		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
		<script src="../js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
		<script src="../css/offcanvas.js"></script>
	</body>
</html>
	