<?php session_start(); 
	if(isset($_SESSION['zalogowany']))
	{require("check.php");}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="../../favicon.ico">

		<title>Rejestracja</title>

		<!-- Bootstrap core CSS -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">

		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="../css/offcanvas.css" rel="stylesheet">

		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="../../assets/js/ie-emulation-modes-warning.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>
		<nav class="navbar navbar-fixed-top navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<a href="index.php"><img class="navbar-brand" src="../img/logo_zagan_taxi.png" alt="Logo Firmy"/></a>
				</div>
		   </div><!-- /.container -->
		</nav><!-- /.navbar -->

		<div class="container">
			<div class="row row-offcanvas row-offcanvas-right">
				<div class="col-xs-12 col-sm-18">
					<p class="pull-right visible-xs"><button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button></p>
					<div class="jumbotron">
						<?php
							require("polacz.php");
								
							if (empty($_POST["imie"]) || empty($_POST["nazwisko"]) || empty($_POST["telefon"]) || empty($_POST["e_mail"]) || empty($_POST["login"]) || empty($_POST["haslo"]) )
							{
								print 	'<div class="alert alert-info">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											<center>Aby założyć konto wypełnij poniższy formularz i kliknij "Zarejestruj się".
										</div>';
									
								print 	'<div class="alert alert-warning	">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
											<center>Aby rejestracja się powiodła, konieczne jest podanie wszystkich danych.
										</div>';
									
								print 	"<center><form class='navbar-form'  method='post'>
											<div class='form-group'>             
												<br><b>Imię:</b><br>
												<input type='text' placeholder='Imię' name='imie' value='' class='form-control'>
											</div>
											<br>
											<div class='form-group'>     
												<br><b>Nazwisko:</b><br>								
												<input type='text' placeholder='Nazwisko' name='nazwisko' value='' class='form-control'>
											</div>
											<br>
											<div class='form-group'> 
												<br><b>Nr telefonu:</b><br>								
												<input type='text' pattern='^\d{9}$' title='Format: 9 cyfr, np. 123456789' placeholder='Nr telefonu' name='telefon' value='' class='form-control'>
											</div>
											<br>
											<div class='form-group'>     
												<br><b>Adres e-mail:</b><br>								
												<input type='email' placeholder='Adres e-mail' name='e_mail' value='	' class='form-control'>
											</div>
											<br>
											<div class='form-group'>    
												<br><b>Login:</b><br>								
												<input type='text' placeholder='Login' name='login' value='' class='form-control'>
											</div>
											<br>
											<div class='form-group'>
												<br><b>Hasło:</b><br>
												<input type='password' placeholder='Hasło' name='haslo' class='form-control'>
											</div>
											<br><br>
											<button type='submit' class='btn btn-primary'><span class='glyphicon glyphicon-user'></span> Zarejestruj się</button>
										</form>";
							}
							else
							{
								$wyznacz_id = "SELECT KLIENCI_ID FROM KLIENCI order by KLIENCI_ID asc";
								$stid = oci_parse($conn, $wyznacz_id);
								ocidefinebyname($stid,"KLIENCI_ID",$klienci_id);
								$r = oci_execute($stid);
										
								while($row = oci_fetch_array($stid, OCI_RETURN_NULLS + OCI_ASSOC))
								{
									$klient_id=$klienci_id;
								}
								$klient_id=$klient_id+1;

								$sprawdz = "SELECT E_MAIL, LOGIN, NR_TELEFONU FROM KLIENCI";
								$stid = oci_parse($conn, $sprawdz);
								ocidefinebyname($stid,"E_MAIL",$sprawdzanyE_mail);
								ocidefinebyname($stid,"LOGIN",$sprawdzanyLogin);
								ocidefinebyname($stid,"NR_TELEFONU",$sprawdzanyNR_TELEFON);
								$r = oci_execute($stid);
								$istniejacyEmail=0;
								$istniejacyTelefon=0;
								$istniejacyLogin=0;
								while($row = oci_fetch_array($stid, OCI_RETURN_NULLS + OCI_ASSOC))
								{
									if($_POST['e_mail']==$sprawdzanyE_mail)
									$istniejacyEmail=1;

									if($_POST['telefon']==$sprawdzanyNR_TELEFON)
										$istniejacyTelefon=1;
											
									if($_POST['login']==$sprawdzanyLogin)
										$istniejacyLogin=1;
								}
								
								if($istniejacyEmail==0 AND $istniejacyTelefon==0 AND $istniejacyLogin==0)
								{
									header("refresh:3;url=index.php");
									print  '<div class="alert alert-success">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<center><strong>Zarejestrowano!</strong> Za chwilkę nastąpi przekierowanie na stronę główną, następnie będzie mógł się zalogowować.
											</div>';
									$nowy_klient="INSERT INTO KLIENCI VALUES ($klient_id,'$_POST[imie]','$_POST[nazwisko]',$_POST[telefon],'$_POST[e_mail]','$_POST[login]','$_POST[haslo]')";
									$nk_stid = oci_parse($conn, $nowy_klient);
									$r_nk = oci_execute($nk_stid);			
								}
								else
								{
									if($istniejacyEmail==1)
									{
										print '<div class="alert alert-danger">
													<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
													<center><strong>Niezarejestrowano!</strong> Podany adres e-mail istnieje już w systemie. Wprowadź inny adres.
												</div>';
									}
												
									if($istniejacyTelefon==1)
									{
										print '<div class="alert alert-danger">
													<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
													<center><strong>Niezarejestrowano!</strong> Podany numer telefonu istnieje już w systemie. Wprowadź inny numer.
												</div>';}
												
									if($istniejacyLogin==1)
									{
										print '<div class="alert alert-danger">
													<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
													<center><strong>Niezarejestrowano!</strong> Podany login istnieje już w systemie. Wprowadź inny login.
												</div>';}
												
										print 	"<center><form class='navbar-form'  method='post'>
													<div class='form-group'>     
														<br><b>Imię:</b><br>								
														<input type='text' placeholder='Imię' name='imie' value='$_POST[imie]' class='form-control'>
													</div>
													<br>
													<div class='form-group'>  
														<br><b>Nazwisko:</b><br>								
														<input type='text' placeholder='Nazwisko' name='nazwisko' value='$_POST[nazwisko]' class='form-control'>
													</div>
													<br>
													<div class='form-group'>   
														<br><b>Nr telefonu:</b><br>								
														<input type='text' pattern='^\d{9}$' title='Format: 9 cyfr, np. 123456789' name='telefon' value='$_POST[telefon]' class='form-control'>
													</div>
													<br>
													<div class='form-group'>   
														<br><b>Adres e-mail:</b><br>								
														<input type='email' placeholder='Adres e-mail' name='e_mail' value='$_POST[e_mail]' class='form-control'>
													</div>
													<br>
													<div class='form-group'> 
														<br><b>Login:</b><br>								
														<input type='text' placeholder='Login' name='login' value='$_POST[login]' class='form-control'>
													</div>
													<br>
													<div class='form-group'>
														<br><b>Hasło:</b><br>
														<input type='password' placeholder='Hasło' name='haslo' value='$_POST[haslo]' class='form-control'>
													</div>
													<br><br>
													<button type='submit' class='btn btn-primary'>Zarejestruj się</button>
												</form>";
								}
							}
						?>
					</div>
				</div><!--/.col-xs-12.col-sm-9-->
			</div><!--/row-->
			<hr>
			<footer>
				<p>Adam Adamczyk 321 IDZ</p>
			</footer>
		</div><!--/.container-->


	    <!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
		<script src="../js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
		<script src="../css/offcanvas.js"></script>
	</body>
</html>
