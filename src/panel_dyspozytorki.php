<?php session_start(); 
	if(isset($_SESSION['zalogowany']))
	{
		if($_SESSION['zalogowany']!=0)
		{	
			require("check.php");
		}
	}
	else{require("check.php");}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="../../favicon.ico">

		<title>Panel dyspozytorki</title>

		<!-- Bootstrap core CSS -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">

		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="../css/offcanvas.css" rel="stylesheet">

		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="../../assets/js/ie-emulation-modes-warning.js"></script>
		  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	 </head>

	<body>
		<nav class="navbar navbar-fixed-top navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<a href="index.php"><img class="navbar-brand" src="../img/logo_zagan_taxi.png" alt="Logo Firmy"/></a>
					<form class="navbar-form navbar-right" method="post">
						<div class="form-group">
							<input type="text" placeholder="Szukaj" name="szukaj" class="form-control">
						</div>	
						<div class="form-group">
							<button type="submit" class="btn btn-info" title="Szukaj" data-toggle="popover" data-trigger="hover" data-content="Wyszukiwarka wyświetla klientów, taksówki bądź kursy odpowiadające szukanemu hasłu."><span class="glyphicon glyphicon-search"></span></a>
						</div>		
					</form>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<form method="POST" action="index.php" class="navbar-form navbar-right">
						<input type='hidden' name='wyloguj' value='TRUE'/>
						<button type="submit" class="btn btn-danger navbar-right"><span class="glyphicon glyphicon-log-out"></span> dyspozytorka - wyloguj się</a>
					</form>
				</div><!--/.navbar-collapse -->
			</div><!-- /.container -->
		</nav><!-- /.navbar -->

		<div class="container">
			<div class="row row-offcanvas row-offcanvas-right">
				<div class="col-xs-12 col-sm-18">
					<p class="pull-right visible-xs">
						<button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
					</p>
					<div class="jumbotron">
						<?php
							require("polacz.php");
							
							if (empty($_POST['zmien_status'])){}
							else
							{
								$zs_stid = oci_parse($conn, $_POST['zmien_status'].$_POST['zmien_status_kursu']);
								$r_zs = oci_execute($zs_stid);
							}
															
							if (empty($_POST['usun_kurs'])){}
							else
							{
								$zs_stid = oci_parse($conn, $_POST['usun_kurs'].$_POST['usun_kurs_id']);
								$r_zs = oci_execute($zs_stid);
							}
													
							if (empty($_POST['szukaj']))
							{		
								print '<ul class="nav nav-pills" role="tablist">';
									if (empty($_POST['ktora_zakladka']))
									{
										print'	<li class="active"><a href="#Klienci" role="tab" data-toggle="tab">Klienci</a></li>
										<li><a href="#Taksówki" role="tab" data-toggle="tab">Taksówki</a></li>
										<li><a href="#Kursy" role="tab" data-toggle="tab">Kursy</a></li>
										<a href="dodaj_taksowke.php" class="btn btn-success navbar-right" role="button"><span class="glyphicon glyphicon-plus-sign"></span> Dodaj taksówkę</a>
										</ul>
										<br>';
									}
									else
									{
										if ($_POST['ktora_zakladka']=='Kursy')
										{
											print'	<li><a href="#Klienci" role="tab" data-toggle="tab">Klienci</a></li>
											<li><a href="#Taksówki" role="tab" data-toggle="tab">Taksówki</a></li>
											<li class="active"><a href="#Kursy" role="tab" data-toggle="tab">Kursy</a></li>
											<a href="dodaj_taksowke.php" class="btn btn-success navbar-right" role="button"><span class="glyphicon glyphicon-plus-sign"></span> Dodaj taksówkę</a>
											</ul>
										  <br>';
										}
									}
								print "<div class='tab-content'>";
											if (empty($_POST['ktora_zakladka']))
												print "<div class='tab-pane active' id='Klienci'>";
											else
											{
												if ($_POST['ktora_zakladka']=='Kursy')
													print "<div class='tab-pane' id='Klienci'>";
											}
													$query = 'SELECT IMIE,NAZWISKO, NR_TELEFONU,E_MAIL FROM KLIENCI WHERE KLIENCI_ID>0 ORDER BY KLIENCI_ID';
													$stid = oci_parse($conn, $query);
													$r = oci_execute($stid);
											
													print '<h2><center>NASI KLIENCI:</h2>';
													print '<center><table class="table table-hover">';
														print '<thead>';
														print '<tr>';
															print '<th><center>Imię</th>';
															print '<th><center>Nazwisko</th>';
															print '<th><center>Nr telefonu</th>';
															print '<th><center>Adres e-mail</th>';			
															print '</tr>';
															print '</thead>';

															while($row = oci_fetch_array($stid, OCI_RETURN_NULLS + OCI_ASSOC)) 
															{
																print '<tr>';
																foreach ($row as $item) 
																{
																	print '<td><center>' .($item !==null ?  htmlentities($item, ENT_QUOTES) : '&nbsp').'</td>';
																}
																print '</tr>';
															}
													print '</table>';
												print "</div>";
													
												print "<div class='tab-pane' id='Taksówki'>";
														$query = 'SELECT P.MARKA, P.MODEL, P.NR_REJESTRACYJNY, P.NR_BOCZNY, P.LICZBA_MIEJSC, KAT.KATEGORIA, KOL.KOLOR, P.KIEROWCA_IMIE, P.KIEROWCA_NAZWISKO
																  FROM POJAZDY P, KATEGORIE_POJAZDOW KAT, KOLORY_POJAZDOW KOL
																  WHERE P.KATEGORIA_ID = KAT.KATEGORIA_ID AND P.KOLOR_ID = KOL.KOLOR_ID AND POJAZDY_ID>0
																  ORDER BY POJAZDY_ID';
														$stid = oci_parse($conn, $query);
														ocidefinebyname($stid,"NR_REJESTRACYJNY",$nr_rejestracyjny);
														ocidefinebyname($stid,"NR_BOCZNY",$nr_boczny);
														$r = oci_execute($stid);
														
														print '<h2><center>NASZE TAKSÓWKI:</h2>';
														print '<center><table class="table table-hover">';
															print '<thead>';
															print '<tr>';
																print '<th><center>Marka</th>';
																print '<th><center>Model</th>';
																print '<th><center>Nr rejestracyjny</th>';
																print '<th><center>Nr boczny</th>';		
																print '<th><center>Liczba miejsc</th>';
																print '<th><center>Kategoria</th>';
																print '<th><center>Kolor</th>';	
																print '<th><center>Imię kierowcy</th>';
																print '<th><center>Nazwisko kierowcy</th>';
																print '<th><center>Zarządzaj</th>';
																print '</tr>';
																print '</thead>';

																while($row = oci_fetch_array($stid, OCI_RETURN_NULLS + OCI_ASSOC)) 
																{
																	print '<tr>';
																		foreach ($row as $item) 
																		{
																			print '<td><center>' .($item !==null ?  htmlentities($item, ENT_QUOTES) : '&nbsp').'</td>';
																		}
																			
																			print '	<form method="post" action="edytuj_taksowke.php">';
																				print "<input type='hidden' name='nr_rejestracyjny' value='$nr_rejestracyjny'/>";
																				print "<input type='hidden' name='nr_boczny' value='$nr_boczny'/>";
																				print '<td><center><button type="submit" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-edit"></span> Edytuj</button></td>
																			</form>';
																	print '</tr>';
																	$_SESSION['taxi']=$nr_rejestracyjny;
																	}
															
															print '</table>';
													print "</div>";
											
													if (empty($_POST['ktora_zakladka']))
															print "<div class='tab-pane' id='Kursy'>";
													else
														{if ($_POST['ktora_zakladka']=='Kursy'){	print "<div class='tab-pane active' id='Kursy'>";}}
																$query = "SELECT K.KURSYID, TO_CHAR(K.DATA,'DD-MM-YYYY'), K.GODZINA, KL.IMIE, KL.NAZWISKO, S.STATUS, U.NAZWA AS SKAD, U2.NAZWA AS DOKAD, P.NR_BOCZNY
																		  FROM POJAZDY P, KLIENCI KL, KURSY K, ULICE U, ULICE U2, STATUSY_KURSOW S
																		  WHERE K.KLIENCI_ID = KL.KLIENCI_ID AND K.STATUS_ID = S.STATUS_ID AND K.SKAD = U.ULICE_ID AND K.DOKAD = U2.ULICE_ID AND P.POJAZDY_ID = K.POJAZDY_ID
																		  ORDER BY K.KURSYID ASC";
																$stid = oci_parse($conn, $query);
																ocidefinebyname($stid,"STATUS",$aktualnyStatus);
																ocidefinebyname($stid,"KURSYID",$aktualneIdKursu);
																ocidefinebyname($stid,"IMIE",$aktualneImie);
																$r = oci_execute($stid);

																print '<h2><center>KURSY:</h2>';
																print '<center><table class="table table-hover">';
																	print '<thead>';
																	print '<tr>';
																	print '<th><center>#</th>';
																	print '<th><center>Data</th>';
																	print '<th><center>Godzina</th>';
																	print '<th><center>Imię klienta</th>';
																	print '<th><center>Nazwisko klienta</th>';	
																	print '<th><center>Status kursu</th>';
																	print '<th><center>Początek kursu</th>';
																	print '<th><center>Koniec kursu</th>';	
																	print '<th><center>Taksówka</th>';	
																	print '<th><center>Zarządzaj</th>';					
																	print '</tr>';
																	print '</thead>';

																	while($row = oci_fetch_array($stid, OCI_RETURN_NULLS + OCI_ASSOC)) 
																	{
																		print '<tr>';
																			foreach ($row as $item) 
																			{
																				print '<td><center>' .($item !==null ?  htmlentities($item, ENT_QUOTES) : '&nbsp').'</td>';
																			}
																			print '	<form method="post" action="zakoncz_kurs.php">';
																						print "<input type='hidden' name='zmien_status_kursu' value='$aktualneIdKursu'>";
																						if($aktualnyStatus=='Oczekujący')
																							print '<td><center><button type="submit" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-edit"></span> Zakończ kurs</button></td>';
																			print '</form>';
																
																			if($aktualnyStatus=='Zakończony')
																			print '<td><center><button class="btn btn-primary btn-xs disabled"><span class="glyphicon glyphicon-edit"></span> Zakończ kurs</button></td>';
																			print 	'<form method="post">';
																				if($aktualnyStatus=='Anulowany')
																					{
																						print '<td><center><button type="submit" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Usuń kurs</button></td>';
																					}
																				$usun_kurs="DELETE FROM KURSY WHERE KURSYID='$aktualneIdKursu'";
																				print "<input type='hidden' name='usun_kurs' value='$usun_kurs'>";
																				print "<input type='hidden' name='usun_kurs_id' value='$aktualneIdKursu'>";
																				print "<input type='hidden' name='ktora_zakladka' value='Kursy'>";
																			print '</form>';	
																		print '</tr>';
																	}
																print '</table>';  						
									
														print "</div>";
										
										print "</div>";
							}
							else
							{
								$szukanaWartosc=$_POST['szukaj'];
								$query = "SELECT IMIE, NAZWISKO, NR_TELEFONU, E_MAIL FROM KLIENCI WHERE NAZWISKO = '$szukanaWartosc' OR IMIE = '$szukanaWartosc' OR TO_CHAR(NR_TELEFONU) = '$szukanaWartosc' OR E_MAIL = '$szukanaWartosc' ORDER BY KLIENCI_ID";
								$zlicz = "SELECT COUNT(*) FROM KLIENCI WHERE NAZWISKO = '$szukanaWartosc' OR IMIE = '$szukanaWartosc' OR TO_CHAR(NR_TELEFONU) = '$szukanaWartosc' OR E_MAIL = '$szukanaWartosc'";
								$stid = oci_parse($conn, $query);
								$r = oci_execute($stid);
								$stid2 = oci_parse($conn, $zlicz);
								$r2 = oci_execute($stid2);
									
								while($row2 = oci_fetch_array($stid2, OCI_RETURN_NULLS + OCI_ASSOC)) 
								{
									print '<tr>';
									foreach ($row2 as $item) 
									{
										$ilewierszy=($item !==null ?  htmlentities($item, ENT_QUOTES) : '&nbsp');
									}
									print '</tr>';
								}
								print '</table>';
								$liczbaZnalezionych=$ilewierszy;
								if ($ilewierszy==0)
								{
									print '';
								}
								else
								{
									print '<h2><center>ZNALEZIENI KLIENCI:</h2>';
									print '<center><table class="table table-hover">';
									print '<thead>';
									print '<tr>';
										print '<th><center>Imię</th>';
										print '<th><center>Nazwisko</th>';
										print '<th><center>Nr telefonu</th>';
										print '<th><center>Adres e-mail</th>';				
									print '</tr>';
									print '</thead>';

									while($row = oci_fetch_array($stid, OCI_RETURN_NULLS + OCI_ASSOC)) 
									{
										print '<tr>';
										foreach ($row as $item) 
										{
											print '<td><center>' .($item !==null ?  htmlentities($item, ENT_QUOTES) : '&nbsp').'</td>';
										}
										print '</tr>';
									}
									print $row;
									print '</table>';
									}
						
									
									$query = "SELECT  P.MARKA, P.MODEL, P.NR_REJESTRACYJNY, P.NR_BOCZNY, TO_CHAR(P.LICZBA_MIEJSC), KAT.KATEGORIA, KOL.KOLOR FROM POJAZDY P, KATEGORIE_POJAZDOW KAT, KOLORY_POJAZDOW KOL WHERE (P.KATEGORIA_ID = KAT.KATEGORIA_ID AND P.KOLOR_ID = KOL.KOLOR_ID AND POJAZDY_ID>0) AND (P.NR_BOCZNY = '$szukanaWartosc' OR P.MARKA = '$szukanaWartosc' OR P.MODEL = '$szukanaWartosc' OR P.NR_REJESTRACYJNY = '$szukanaWartosc' OR TO_CHAR(LICZBA_MIEJSC)='$szukanaWartosc' OR KAT.KATEGORIA = '$szukanaWartosc' OR KOL.KOLOR = '$szukanaWartosc') ORDER BY POJAZDY_ID";
									$zlicz = "SELECT COUNT(*) FROM POJAZDY P, KATEGORIE_POJAZDOW KAT, KOLORY_POJAZDOW KOL WHERE (P.KATEGORIA_ID = KAT.KATEGORIA_ID AND P.KOLOR_ID = KOL.KOLOR_ID AND POJAZDY_ID>0) AND (P.NR_BOCZNY = '$szukanaWartosc' OR P.MARKA = '$szukanaWartosc' OR P.MODEL = '$szukanaWartosc' OR P.NR_REJESTRACYJNY = '$szukanaWartosc' OR TO_CHAR(P.LICZBA_MIEJSC)='$szukanaWartosc' OR KAT.KATEGORIA = '$szukanaWartosc' OR KOL.KOLOR = '$szukanaWartosc')";
									$stid = oci_parse($conn, $query);
									$r = oci_execute($stid);
									$stid2 = oci_parse($conn, $zlicz);
									$r2 = oci_execute($stid2);
										
									while($row2 = oci_fetch_array($stid2, OCI_RETURN_NULLS + OCI_ASSOC)) 
									{
										print '<tr>';
										foreach ($row2 as $item) 
										{
											$ilewierszy=($item !==null ?  htmlentities($item, ENT_QUOTES) : '&nbsp');
										}
										print '</tr>';
									}
									print '</table>';
									$liczbaZnalezionych=$liczbaZnalezionych+$ilewierszy;
									if ($ilewierszy==0)
									{
												print '';
									}
									else
									{
										print '<h2><center>ZNALEZIONE TAKSÓWKI:</h2>';
										print '<center><table class="table table-hover">';
											print '<thead>';
											print '<tr>';
												print '<th><center>Marka</th>';
												print '<th><center>Model</th>';
												print '<th><center>Nr rejestracyjny</th>';
												print '<th><center>Nr boczny</th>';	
												print '<th><center>Liczba miejsc</th>';
												print '<th><center>Kategoria</th>';
												print '<th><center>Kolor</th>';	
												print '</tr>';
												print '</thead>';

												while($row = oci_fetch_array($stid, OCI_RETURN_NULLS + OCI_ASSOC)) 
												{
													print '<tr>';
													foreach ($row as $item) 
													{
														print '<td><center>' .($item !==null ?  htmlentities($item, ENT_QUOTES) : '&nbsp').'</td>';
													}
												print '</tr>';
												}
											
											print $row;
										print '</table>';
									}
							
									$query = "SELECT TO_CHAR(K.DATA,'DD-MM-YYYY'), K.GODZINA, KL.IMIE, KL.NAZWISKO, S.STATUS, U.NAZWA AS SKAD, U2.NAZWA AS DOKAD, P.NR_BOCZNY
											  FROM POJAZDY P, KLIENCI KL, KURSY K, ULICE U, ULICE U2, STATUSY_KURSOW S
											  WHERE (K.KLIENCI_ID = KL.KLIENCI_ID AND K.STATUS_ID = S.STATUS_ID AND K.SKAD = U.ULICE_ID AND K.DOKAD = U2.ULICE_ID AND P.POJAZDY_ID = K.POJAZDY_ID) AND (K.GODZINA = '$szukanaWartosc' OR KL.IMIE = '$szukanaWartosc' OR KL.NAZWISKO = '$szukanaWartosc' OR TO_CHAR(K.DATA,'DD-MM-YYYY') = '$szukanaWartosc' OR S.STATUS = '$szukanaWartosc' OR P.NR_BOCZNY = '$szukanaWartosc' OR U.NAZWA = '$szukanaWartosc' OR U2.NAZWA = '$szukanaWartosc') ORDER BY KURSYID";
									$zlicz = "SELECT COUNT(*) FROM POJAZDY P, KLIENCI KL, KURSY K, ULICE U, ULICE U2, STATUSY_KURSOW S
											  WHERE (K.KLIENCI_ID = KL.KLIENCI_ID AND K.STATUS_ID = S.STATUS_ID AND K.SKAD = U.ULICE_ID AND K.DOKAD = U2.ULICE_ID AND P.POJAZDY_ID = K.POJAZDY_ID) AND (K.GODZINA = '$szukanaWartosc' OR KL.IMIE = '$szukanaWartosc' OR KL.NAZWISKO = '$szukanaWartosc' OR TO_CHAR(K.DATA,'DD-MM-YYYY') = '$szukanaWartosc' OR S.STATUS = '$szukanaWartosc' OR P.NR_BOCZNY = '$szukanaWartosc' OR U.NAZWA = '$szukanaWartosc' OR U2.NAZWA = '$szukanaWartosc')";
									
									$stid = oci_parse($conn, $query);
									$r = oci_execute($stid);
									$stid2 = oci_parse($conn, $zlicz);
									$r2 = oci_execute($stid2);
										
									while($row2 = oci_fetch_array($stid2, OCI_RETURN_NULLS + OCI_ASSOC)) 
									{
										print '<tr>';
										foreach ($row2 as $item) 
										{
											$ilewierszy=($item !==null ?  htmlentities($item, ENT_QUOTES) : '&nbsp');
										}
										print '</tr>';
									}
									print '</table>';
									$liczbaZnalezionych=$liczbaZnalezionych+$ilewierszy;
										
									if ($liczbaZnalezionych==0)
									{
										print '<div class="alert alert-danger">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<center>Niestety, nic nie znaleziono.
											</div>';	
									}
									else
									{
										if($ilewierszy!=0)
										{
											print '<h2><center>ZNALEZIONE KURSY:</h2>';
											print '<center><table class="table table-hover">';
												print '<thead>';
												print '<tr>';
													print '<th><center>Data</th>';
													print '<th><center>Godzina</th>';
													print '<th><center>Imię klienta</th>';
													print '<th><center>Nazwisko klienta</th>';	
													print '<th><center>Status kursu</th>';
													print '<th><center>Początek kursu</th>';
													print '<th><center>Koniec kursu</th>';	
													print '<th><center>Taksówka</th>';				
													print '</tr>';
													print '</thead>';

													while($row = oci_fetch_array($stid, OCI_RETURN_NULLS + OCI_ASSOC)) 
													{
														print '<tr>';
														foreach ($row as $item) 
														{
															print '<td><center>' .($item !==null ?  htmlentities($item, ENT_QUOTES) : '&nbsp').'</td>';
														}
														print '</tr>';
													}
												print $row;
											print '</table>';
										}
									}
									print '<center><a href="panel_dyspozytorki.php" class="btn btn-default" role="button"><span class="glyphicon glyphicon-home"></span> Wróć do panelu</a>';
							}
							oci_close($conn);
						?>
					</div>
				</div>
			</div>
			<hr>
			<footer>
					<p>Adam Adamczyk 321 IDZ</p>
			</footer>
		</div><!--/.container-->
		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
		<script src="../js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
		<script src="../css/offcanvas.js"></script>
		<script>
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover();   
		});
		</script>
	</body>
</html>
