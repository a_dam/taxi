<?php session_start(); 
	unset($_SESSION['zalogowany']);
	unset($_SESSION['kto']);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Żagań Taxi - Home Page</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/jumbotron.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	</head>

	<body>
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<a href="index.php"><img class="navbar-brand" src="../img/logo_zagan_taxi.png" alt="Logo Firmy"/></a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
				<form class="navbar-form navbar-right" action="logowanie.php" method="post">
					<div class="form-group">             
						<input type="email"  placeholder="Adres e-mail" name="e-mail" value="" class="form-control">
					</div>
					<div class="form-group">
						<input type="password" placeholder="Hasło" name="haslo" class="form-control">
					</div>
					<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-log-in"></span> Zaloguj się</button>
					<a href="rejestracja.php" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-user"></span> Zarejestruj się</a>
				</form>
				</div><!--/.navbar-collapse -->
			</div>
		</nav>

		<!-- Main jumbotron for a primary marketing message or call to action -->
		<div class="jumbotron">
		
			<div class="container">
				<center><img src="../img/logo_zagan_taxi.png" alt="Logo Firmy"/>
				<center><h1>Żagań Taxi - najszybciej w mieście</h1>
			</div>
			<br>
			<div class="container">
				<center><h2><font color="#4574f7">BEZPIECZNE TAXI</h1></font>
				<!-- Example row of columns -->
				<div class="row">
					<div class="col-md-4">
						<center><img src="../img/licencjonowani_taksowkarze.png" alt="Logo Firmy"/>	
						<h3>Licencjonowani taksówkarze</h3>
						<h5>Kierowcy Żagań Taxi to licencjonowani taksówkarze z wieloletnim doświadczeniem i doskonałą znajomością topografii Żagania.</h5>
					</div>
					<div class="col-md-4">
						<center><img src="../img/regularny_serwis.png" alt="Logo Firmy"/>	
						<h3>Regularny serwis</h3>
						<h5>Regularnie serwisujemy naszą flotę taksówek i co najmniej raz w miesiącu sprawdzamy stan techniczny każdego pojazdu.</h5>
					</div>
					<div class="col-md-4">
						<center><img src="../img/bezpieczenstwo.png" alt="Logo Firmy"/>	
						<h3>Bepieczeńśtwo</h3>
						<h5>Bezpieczeństwo naszych klientów jest dla nas sprawą priorytetową, dlatego wszyscy kierowcy przechodzą badania psychologiczne i sprawdzamy ich niekaralność.</h5>
					</div>
				</div>
				<br>
				<center><h2><font color="#009e60">KOMFORTOWE TAXI</h1></font>
				<!-- Example row of columns -->
				<div class="row">
					<div class="col-md-4">
						<center><img src="../img/zaladunek_bagazy.png" alt="Logo Firmy"/>	
						<h3>Załadunek bagaży</h3>
						<h5>Nasi kierowcy zawsze pomogą w włożeniu i wyjęciu bagaży.</h5>
					</div>
					<div class="col-md-4">
						<center><img src="../img/klimatyzacja.png" alt="Logo Firmy"/>	
						<h3>Klimatyzacja</h3>
						<h5>Każdy pojazd Żagań Taxi jest klimatyzowany, aby zapewnić naszym pasażerom jak najbardziej komfortowe warunki podróży.</h5>
					</div>
					<div class="col-md-4">
					<center><img src="../img/czystosc.png" alt="Logo Firmy"/>	
						<h3>Czystość</h3>
						<h5>Czystość floty Żagań Taxi jest kontrolowana nawet 2 razy w tygodniu.</h5>
					</div>
				</div>
			</div>	
			<br>
			<center><h2>JEŹDZIMY SAMOCHODAMI</h1>
			<div class="container">
				<br>
				<div id="myCarousel" class="carousel slide" data-interval="1500" data-ride="carousel">
							<!-- Indicators -->
					

						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<center><img src="../img/renault.png" alt="Renault" width="250" height="250"></center>								
							</div>

							<div class="item">
								<center><img src="../img/audi.png" alt="Audi" width="250" height="250"></center>								
							</div>
						
							<div class="item">
								<center><img src="../img/fiat.png" alt="Fiat" width="250" height="250"></center>								
							</div>
							
							<div class="item">
								<center><img src="../img/ford.png" alt="Ford" width="250" height="250"></center>								
							</div>
							
							<div class="item">
								<center><img src="../img/vw.png" alt="Volkswagen" width="250" height="250"></center>								
							</div>
							
							<div class="item">
								<center><img src="../img/peugeot.png" alt="Peugeot" width="250" height="250"></center>								
							</div>
							
							<div class="item">
								<center><img src="../img/bmw.png" alt="BMW" width="250" height="250"></center>								
							</div>
							
							<div class="item">
								<center><img src="../img/mercedes.png" alt="Mercedes" width="250" height="250"></center>								
							</div>
						</div>

						<!-- Left and right controls -->
						<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
							  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							  <span class="sr-only">Next</span>
						</a>
					</div>
			</div>
		</div>
		
		<div class="container">
			<hr>
			<footer>
				<p>Adam Adamczyk 321 IDZ </p>
			</footer>
		</div> <!-- /container -->
		
		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
		<script src="../js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
	</body>
</html>
